import React from 'react';
import { Link } from 'react-router';
import Activities from '../Common/components/Activities';
import ActivitiesRows from '../Common/components/ActivitiesRows';
import Activity from '../Common/components/Activity';
import ActivityService from '../Common/services/Activity';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activities: []
    };

    ActivityService.getActivities().then(this.saveActivities.bind(this));
  }

  saveActivities(activities) {
    this.setState({'activities': activities});
  }

  render() {
    const activitiesProps = {
      activities: this.state.activities
    };

    const activitiesProps2 = {
      displayImage: false,
      activities: this.state.activities.slice(0, 3)
    };

    const activityProps = this.state.activities[0];

    return (
      <div className='Feed'>
        <div className="col-xs-12">
          <div className="col-xs-12">
            <h3>Portal</h3>
          </div>

          <div className="col-xs-6 inline-activities">
            <h4>Activitatile la care particip</h4>

            <ActivitiesRows {...activitiesProps2} />
          </div>

          <div className="col-xs-6">
            <h4>Activitati proprii</h4>

            <ActivitiesRows {...activitiesProps2} />

            <Link to="activity-new">
              <button className="btn btn-success">Creeaza activitate</button>
            </Link>
          </div>

          <div className="col-xs-12">
            <h4>Hot today</h4>

            <div className="col-sm-8 col-sm-push-2">
              <Activity {...activityProps} />
            </div>
          </div>

          <div className="col-xs-12">
            <h4>Restul activitatilor</h4>
            <Activities {...activitiesProps} />
          </div>
        </div>

        <div className="clearfix"></div>
      </div>
    );
  }

}

export default App;
