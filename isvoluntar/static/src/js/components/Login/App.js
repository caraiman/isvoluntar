import React from 'react';
import Activities from '../Common/components/Activities';

class Login extends React.Component {

  render() {
    const activitiesProps = {
      displayReward: false,
      activities: [
      {
        title: 'Poti colectiona puncte',
        description: 'Oricare activitate a ta este rasplatita de organizatori. La randul tau, poti organiza activitati folosind din punctele tale',
        picture: '/assets/coins.jpg'
      },
      {
        title: 'Clasamente',
        description: 'Intrecete cu ceilalti voluntari pentru a ajunge pe podium si a primi distinctii speciale',
        picture: '/assets/podium.jpg'
      },
      {
        title: 'Activitati',
        description: 'Diversitatea si lucrul in echipa sunt doua din lucrurile pe care se conenctreaza platforma noastra',
        picture: '/assets/volunteer.jpg'
      }]
    };

    return (
      <div className="Login">
        <div className="col-sm-8 col-sm-push-2">
          <h3 className="title">Autentificare</h3>
          <div className="col-sm-12 no-padding">
            <button className="btn btn-success">Login with Facebook</button>
            <button className="btn btn-success">Login with Google</button>
          </div>

          <div className="col-xs-12">
            <hr />
          </div>

          <div className="col-xs-12 no-padding">
            <div className="advantages">
              <h4>Avantajele unui cont</h4>

              <Activities {...activitiesProps} />
            </div>
          </div>
        </div>

        <div className="clearfix"></div>
      </div>
    );
  }

}

export default Login;
