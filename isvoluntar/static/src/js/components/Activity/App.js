import React from 'react';
import Jumbotron from '../Common/components/Jumbotron';
import Map from '../Common/components/Map';
import ActivityService from '../Common/services/Activity';

class Activity extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activity: {}
    };

    ActivityService.getActivity(this.props.params.id).then(this.saveActivity.bind(this));
  }

  saveActivity(activity) {
    this.setState({activity: activity});
  }

  generateActivity(activity) {
    const jumbotronProps = {
      type: 'image',
      title: activity.title,
      picture: activity.picture
    };

    const divStyle = {
      backgroundImage: `url('${jumbotronProps.picture}')`
    };

    return (
      <div>
        <div className="img-container" style={divStyle}></div>

        <div className="col-sm-8 col-sm-push-2 content">
          <div className="col-sm-7 details">
            <div className="title">{activity.title}</div>
            <div className="description">{activity.description}</div>
            <div className="date">{activity.date}</div>
            <div className="reward">Reward: {activity.reward}</div>
            <button className="btn btn-success">Vreau sa particip</button>
          </div>

          <div className="col-sm-4 col-sm-push-1">
            <Map {...activity} />
          </div>
        </div>

        <div className="clearfix"></div>
      </div>
    );

  }

  render() {
    return (
      <div className="Activity-Page">
        { this.generateActivity(this.state.activity) }
      </div>
    );
  }

}

export default Activity;
