import React from 'react';
import Jumbotron from '../Common/components/Jumbotron';
import Map from '../Common/components/Map';
import ActivityService from '../Common/services/Activity';

class ActivityNew extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activity: {}
    };
  }

  handleChange(name, event) {
    let activity = this.state.activity;

    activity[name] = event.target.value;

    this.setState({ activity: activity });
  }

  submit() {

    return ActivityService.createActivity(this.state.activity);

  }

  generateActivity(activity) {
    const mapProps = {

    };

    return (
      <div>
        <div className="col-sm-8 col-sm-push-2 content">
          <div className="col-xs-12 details">
            <h4>Activitate noua</h4>
            <hr />

            <h5>Titlu</h5>
            <input className="form-control" type="text" value={this.state.title} onChange={this.handleChange.bind(this, 'title')} />

            <h5>Data</h5>
            <input className="form-control" type="date" value={this.state.date} onChange={this.handleChange.bind(this, 'date')} />

            <h5>Descriere</h5>
            <textarea className="form-control" value={this.state.description} onChange={this.handleChange.bind(this, 'description')}></textarea>

            <h5>Puncte oferite (per participant)</h5>
            <input className="form-control" type="number" onChange={this.state.reward} onChange={this.handleChange.bind(this, 'reward')} />

            <h5>Numar participanti </h5>
            <input className="form-control" type="number" onChange={this.state.helpers} onChange={this.handleChange.bind(this, 'helpers')} />

            <h5>Locatie</h5>
            <Map {...mapProps} />

            <button className="btn btn-success pull-right" onClick={this.submit.bind(this)}>Creeaza activitate</button>
          </div>
        </div>

        <div className="clearfix"></div>
      </div>
    );
  }

  render() {
    return (
      <div className="Activity-Page Activity-Page-New">
        { this.generateActivity(this.props) }
      </div>
    );
  }

}

export default ActivityNew;
