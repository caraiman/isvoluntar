import React from 'react';
import Jumbotron from '../Common/components/Jumbotron';
import ThreeColumns from '../Common/components/ThreeColumns';
import Activities from '../Common/components/Activities';
import ActivityService from '../Common/services/Activity';

class Home extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      activities: []
    };

    ActivityService.getActivities().then(this.saveActivities.bind(this));
  }

  saveActivities(activities) {
    this.setState({'activities': activities});
  }

  render() {
    const opt = {
      title: 'IaşiAjută',
      description: 'Schimbam modul in care faci voluntariat',
      secondDescription: 'Saeu sunt cel mai tare pirat',
      findMore: 'Afla la mai multe mai jos',
      type: 'video'
    };

    const threeColumnsProps = {
      items: [
        {
          title: 'Concentrati in jurul utilizatorilor',
          description: 'Comunitatea este inima platformei noastre',
          icon: 'fa fa-users'
        },
        {
          title: 'Iti oferim o motivatie',
          description: 'Poate ideeas de a ii ajuta pe altii nu te incanta. Noi vom incerca sa schimbam asta',
          icon: 'fa fa-sun-o'
        },
        {
          title: 'Aplicatia pe mobil',
          description: 'Fii la curent cu toate activitatile postate, ca sa poti fi printre primii care se implica',
          icon: 'fa fa-phone'
        }
      ]
    };

    const threeColumnsProps2 = {
      items: [
        {
          title: 'Pasul 1',
          description: 'Te inregistrezi'
        },
        {
          title: 'Pasul 2',
          description: 'Participi la cateva activitati ca sa faci puncte',
        },
        {
          title: 'Pasul 3',
          description: 'Deja ti-ai facut prieteni noi pe care-i poti chema la evenimentele tale',
        }
      ]
    };

    const activitiesProps = {
      activities: this.state.activities
    };

    return (
      <div className="home">
        <Jumbotron {...opt} />
        <div className="last col-xs-12">
          <div className="col-xs-12">
            <h4>Platforma de gamification a voluntariatului</h4>
            <span>Iti oferim o solutie aosndoaisnd oiasd oiasnd oiasndoi ansdoina sodnaso idnaspodn asoidn asoindsa pondosaind aspoindiosa ndoasind oaindasoid</span>
          </div>
        </div>
        <hr className="small" />

        <ThreeColumns {...threeColumnsProps} />

        <hr className="small" />

        <div className="last col-xs-12">
          <div className="col-xs-12">
            <h4>Ultimele activitati adaugate</h4>
          </div>
        </div>

        <div className="clearfix"></div>

        <Activities {...activitiesProps} />
        <hr className="small" />

        <div className="last col-xs-12">
          <div className="col-xs-12">
            <h4>Cum poti sa te implici ?</h4>
          </div>
        </div>

        <ThreeColumns {...threeColumnsProps2} />
      </div>
    );
  }
}

export default Home;
