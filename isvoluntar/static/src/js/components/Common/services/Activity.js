var request = require('superagent');

const createActivity = (activity) => {

  const promise = new Promise((resolve, reject) => {
    request
    .post('http://elbear.com/api/activities/')
    .send(activity)
    .end(function(err, res) {
      if (err) {
        reject(err);
      } else {
        resolve(res.body);
      }
    });
  });

};

const getActivity = (id) => {

  const promise = new Promise((resolve, reject) => {
    request.get('http://elbear.com/api/activities/' + id).end(function(err, res) {
      if (err) {
        reject(err);
      } else {
        resolve(res.body);
      }
    });
  });

  return promise;

};

const getActivities = () => {

  const promise = new Promise((resolve, reject) => {
    request.get('http://elbear.com/api/activities/').end(function(err, res) {
      if (err) {
        reject(err);
      } else {
        resolve(res.body);
      }
    });
  });

  return promise;

};

const ActivityService = {
  createActivity: createActivity,
  getActivities: getActivities,
  getActivity: getActivity
};

export default ActivityService;
