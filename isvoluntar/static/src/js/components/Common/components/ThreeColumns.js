import React from 'react';

class ThreeColumns extends React.Component {

  generateItems(items) {
    let res = [];

    items.map((item) => {
      let icon = null;

      if (item.icon) {
        icon = <i className={item.icon}></i>;
      } else if (item.iconContent) {
        icon = <span>{item.iconContent}</span>;
      }

      res.push(
        <div className="item col-sm-4">
          <div className="item__icon">
            { icon }
          </div>

          <div className="item__title">
            <span>{ item.title }</span>
          </div>

          <div>
            <span>{ item.description }</span>
          </div>
        </div>
      );
    });

    return res;
  }

  render() {
    return (
      <div className="ThreeColumns">
        <div className="ThreeColumns__items">
          { this.generateItems(this.props.items) }
        </div>

        <div className="clearfix"></div>
      </div>
    );
  }

}

export default ThreeColumns;
