import React from 'react';

class ActivityRow extends React.Component {

  render() {
    let displayImage = true;
    let displayReward = true;

    if (this.props.displayImage === false) {
      displayImage = false;
    }

    if (this.props.displayReward === false) {
      displayReward = false;
    }

    return (
      <div className="ActivityRow">
        <p className="card-text title">{ this.props.title }</p>
        <p className="card-text">{ this.props.description.substr(0, 100) }...</p>

        <div className="reward">
          <p className="card-text" hidden={!displayReward}>{ 200 }</p>
        </div>

        <div className="clearfix"></div>
      </div>
    );
  }

}

export default ActivityRow;
