import React from 'react';

class Footer extends React.Component {

  generateFooter(footer) {
    return (
      <footer>
        <div className="col-xs-12 footer__general">
          <div>
            <span>Platforma construita pentru a facilita participarea la evenimente.</span>
          </div>
          <div>
            <span>Lorem ipsum, ceva ceva ceva, salut, unu doi trei</span>
          </div>
        </div>

        <div className="col-sm-4 footer_contact footer__column">
          <span className="title">Contact</span>

          <div className="contact__section">
            <i className="fa fa-phone"></i>
            <span>  +40740 270 895</span>
          </div>
          <div className="contact__section">
            <i className="fa fa-envelope-o"></i>
            <span>  contact@iasiajuta.ro</span>
          </div>
          <div className="contact__section">
            <i className="fa fa-map-marker"></i>
            <span>  Str. Unirii nr. 7</span>
          </div>
        </div>
        <div className="col-sm-4 footer__column">
          <span className="title">Email</span>

          <div>
            <span className="contact__section">Ai intrebari sau ai un ONG si vrei sa te ajutam sa-ti creezi compania in cadrul platformei noastre ? Scrie-ne un mail, iti vom raspunde cat mai prompt posibil.</span>
            <input className="form-control" type="text" placeholder="Email" />
            <input className="form-control" type="text" placeholder="Subiect" />
            <textarea className="form-control" placeholder="Mesajul tau"></textarea>
            <button className="btn btn-success pull-right" type="button">
              Trimite mail
            </button>
          </div>
        </div>
        <div className="col-sm-4 footer__column">
          <span className="title">Copyright</span>

          <div>
            <span className="contact__section">Realizat la Code4Iasi, de Lucian U., Alex, Stefan</span>
          </div>
        </div>

        <div className="clearfix"></div>
      </footer>
    );
  }

  render() {
    return (
      <div>
        { this.generateFooter() }
      </div>
    );
  }
}

export default Footer;
