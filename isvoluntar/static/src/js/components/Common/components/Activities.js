import React from 'react';
import Activity from './Activity';

class Activities extends React.Component {

  componentDidMount() {
    const width = $(document).width();

    $(".has-card-columns").removeClass("card-columns");

    if (width > 700) {
      $(".has-card-columns").addClass("card-columns");
    }
  }

  generateActivities(prop) {
    let res = [];
    let activities = prop.activities;

    activities.map((activity, index) => {
      activity.displayImage = prop.displayImage;
      activity.displayReward = prop.displayReward;

      res.push(
        <Activity key={`activity-${index}`} {...activity} />
      );
    });

    return (
      <div className="has-card-columns">
        { res }
      </div>
    )
  }

  render() {
    return(
      <div className="Activities">
        { this.generateActivities(this.props) }

        <div className="clearfix"></div>
      </div>
    );
  }

}

export default Activities;
