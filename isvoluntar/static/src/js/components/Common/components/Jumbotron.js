import React from 'react';

class Jumbotron extends React.Component {

  generateDefaultJumbotron(jumbotron) {
    return (
      <div className="jumbotron">
        <h1 className="display-3">{ jumbotron.title }</h1>
        <p className="lead">{ jumbotron.description }</p>

        <hr className="m-y-md" />

        <p className="lead">{ jumbotron.secondDescription }</p>

        <p className="lead">
          <button className="btn btn-success">{ jumbotron.findMore }</button>
        </p>
      </div>
    );
  }

  generateImageJumbotron(jumbotron) {
    const jumbotronStyle = {
      backgroundImage: `url('${jumbotron.picture}')`
    };

    return (
      <div className="jumbotron jumbotron--imagewithtitle" style={jumbotronStyle}>
        <h2 className="title display-2">{ jumbotron.title }</h2>
      </div>
    );
  }

  generateVideoJumbotron(jumbotron) {
    return (
      <div>
        <div className="jumbotron jumbotron--video">
          <div className="jumbotron__title">
            <h1 className="title">{ jumbotron.title }</h1>
            <p className="lead">{ jumbotron.description }</p>
            <p className="lead">
              <button className="btn btn-success sharp">{ jumbotron.findMore }</button>
            </p>
          </div>

          <video autoPlay loop className="jumbotron__video">
            <source src="assets/video.mp4" type="video/mp4" />
            <source src="assets/video.webm" type="video/webm" />
          </video>
        </div>
      </div>
    );
  }

  render() {
    const props = this.props;

    if (props.type == 'image') {
      return this.generateImageJumbotron(props);
    }

    if (props.type == 'video') {
      return this.generateVideoJumbotron(props);
    }

    return this.generateDefaultJumbotron(props);
  }

}

export default Jumbotron;
