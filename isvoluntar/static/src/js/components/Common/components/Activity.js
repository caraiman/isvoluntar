import React from 'react';
import { Link } from 'react-router';

class Activity extends React.Component {

  render() {
    let displayImage = true;
    let displayReward = true;

    if (this.props.displayImage === false) {
      displayImage = false;
    }

    if (this.props.displayReward === false) {
      displayReward = false;
    }

    const url = `activity/${this.props.id}`;

    return (
      <div className="Activity">
        <Link to={url}>
          <div className="card">
            <img src={ this.props.picture } hidden={!displayImage} />

            <div className="card-block">
              <p className="card-text title">{ this.props.title }</p>
              <p className="card-text">{ this.props.description }</p>
              <p className="card-text" hidden={!displayReward}>{ 200 }</p>
            </div>
          </div>
        </Link>

        <div className="clearfix"></div>
      </div>
    );
  }

}

export default Activity;
