import React from 'react';
import { Link } from 'react-router';

class Navbar extends React.Component {

  render() {
    return (
      <nav className="navbar navbar-fixed-top">
        <button className="navbar-toggler hidden-sm-up pull-right" data-toggle="collapse" data-target="#navbarCollapse">
          &#9776;
        </button>

        <a className="navbar-brand" href="#/home">IaşiAjută</a>

        <div className="collapse navbar-toggleable-xs" id="navbarCollapse">
          <ul className="nav navbar-nav pull-right">
            <li className="nav-item">
              <Link to="/login">
                <button className="btn btn-success">Login</button>
              </Link>
            </li>
          </ul>
        </div>
      </nav>
    );
  }

}

export default Navbar;
