import React from 'react';
import ActivityRow from './ActivityRow';

class ActivitiesRows extends React.Component {

  generateActivities(prop) {
    let res = [];
    let activities = prop.activities;

    activities.map((activity, index) => {
      activity.displayImage = prop.displayImage;
      activity.displayReward = prop.displayReward;

      res.push(
        <ActivityRow key={`activity-${index}`} {...activity} />
      );
    });

    return (
      <div>
        { res }
      </div>
    )
  }

  render() {
    return(
      <div className="ActivitiesRows">
        { this.generateActivities(this.props) }

        <div className="clearfix"></div>
      </div>
    );
  }

}

export default ActivitiesRows;
