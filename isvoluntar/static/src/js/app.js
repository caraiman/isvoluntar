import React from 'react';
import ReactDOM from 'react-dom';
import { DefaultRoute, Link, Route, RouteHandler, Router, IndexRoute } from 'react-router';
import Login from './components/Login/App';
import Home from './components/Home/App';
import Feed from './components/Feed/App';
import Activity from './components/Activity/App';
import ActivityNew from './components/ActivityNew/App';
import Footer from './components/Common/components/Footer';
import Navbar from './components/Common/components/Navbar';
import { createHistory, useBasename } from 'history';

const history = createHistory();

export default class App extends React.Component {

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <Navbar />
          { this.props.children }
          <Footer />
        </div>
      </div>
    );
  }

}

ReactDOM.render((
  <Router>
    <Route path='/' component={App}>
      <Route path='login' component={Login} />
      <Route path='home' component={Home} />
      <Route path='feed' component={Feed} />
      <Route path='activity/:id' component={Activity} />
      <Route path='activity-new' component={ActivityNew} />
    </Route>
  </Router>
), document.getElementById('react'));

$(window).resize(function() {
  checkCards();
})

$(document).ready(function() {
  checkCards();
});

function checkCards() {
  const width = $(document).width();

  if (width > 700) {
    $(".has-card-columns").addClass("card-columns");
  } else {
    $(".has-card-columns").removeClass("card-columns");
  }
}
