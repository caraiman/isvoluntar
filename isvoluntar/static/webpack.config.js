var path = require('path');
var webpack = require('webpack');

var config = {
  entry: [
    'webpack/hot/only-dev-server',
    './src/js/app.js'
  ],
  output: {
    path: __dirname + '/dist',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      { test: /\.js$/, loader: 'babel', exclude: /node_modules/, query: { presets: ['react', 'es2015'] } }
    ]
  },
  plugins: [
    new webpack.NoErrorsPlugin()
  ]
};

module.exports = config;
