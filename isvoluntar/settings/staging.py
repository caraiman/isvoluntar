from isvoluntar.settings.common import *

DEBUG = False

DATABASES['default'].update({
    'NAME': 'isvoluntar',
    'USER': 'isvoluntar',
})

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211',
    },
}

ALLOWED_HOSTS = [
    '.',
]
