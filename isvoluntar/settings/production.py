from isvoluntar.settings.common import *

DEBUG = False

DATABASES['default'].update({
    'NAME': 'isvoluntar',
    'USER': 'isvoluntar',
})

ALLOWED_HOSTS = [
    '.elbear.com',
]
