from isvoluntar.settings.common import *


DATABASES['default'].update({
    'NAME': 'isvoluntar',
})

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INSTALLED_APPS += (
    'debug_toolbar',
)

INTERNAL_IPS = (
    '127.0.0.1',
)

try:
    from isvoluntar.settings.local import *
except ImportError:
    pass
