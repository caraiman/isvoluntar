from django.conf.urls import url

from profiles.views import ProfileDetail, ProfileList


urlpatterns = [
    url(r'^profiles/$', ProfileList.as_view(), name='profile-list'),
    url(r'^profiles/(?P<pk>[0-9]+)/$', ProfileDetail.as_view(), name='profile-detail'),
]