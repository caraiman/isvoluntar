from django.conf import settings
from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    points = models.IntegerField(default=100)

    def __unicode__(self):
        return "%s:%s" % (self.user.username, self.points)
