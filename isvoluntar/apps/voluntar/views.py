import json

from django.apps import apps
from django.conf import settings

from rest_framework import generics, permissions, status
from rest_framework.response import Response

from voluntar.models import Activity, Attendance
from voluntar.serializers import ActivitySerializer, AttendanceSerializer


class ActivityList(generics.ListCreateAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = (permissions.IsAuthenticated,)


class ActivityDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = (permissions.IsAuthenticated,)


class AttendanceCreate(generics.CreateAPIView):
    queryset = Attendance.objects.all()

    def create(self, request, *args, **kwargs):
        data = json.loads(request.data)
        user_id = data['user_id']
        user = apps.get_model(settings.AUTH_USER_MODEL).objects.get(id=user_id)
        activity_id = data['activity_id']
        activity = Activity.objects.get(id=activity_id)
        Attendance.objects.create(user=user, activity=activity)
        headers = self.get_success_headers(request.data)
        return Response(request.data, status=status.HTTP_201_CREATED, headers=headers)


class AttendanceUpdate(generics.UpdateAPIView):
    queryset = Attendance.objects.all()
    lookup_field = 'user__id'
    lookup_url_kwarg = 'uid'
    serializer_class = AttendanceSerializer
    permission_classes = (permissions.IsAuthenticated,)
