from django.contrib import admin

from voluntar.models import Activity, Attendance


admin.site.register(Activity)
admin.site.register(Attendance)
