from django.conf import settings
from django.db import models


class Activity(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='activities')
    reward = models.IntegerField()
    title = models.CharField(max_length=255)
    description = models.TextField()
    date = models.DateTimeField()
    picture = models.CharField(max_length=255)
    lat = models.FloatField(blank=True, null=True)
    lon = models.FloatField(blank=True, null=True)
    helpers = models.IntegerField(default=1)
    attendees = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                       through='Attendance',
                                       related_name='attendances')

    def __unicode__(self):
        return "<%s:%s>" % (self.title[:30], self.reward)


class Attendance(models.Model):
    activity = models.ForeignKey(Activity)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    present = models.BooleanField(default=False)
