# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('voluntar', '0003_activity_helpers'),
    ]

    operations = [
        migrations.CreateModel(
            name='Attendance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('present', models.BooleanField(default=False)),
                ('activity', models.ForeignKey(to='voluntar.Activity')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='activity',
            name='attendees',
            field=models.ManyToManyField(related_name='attendances', through='voluntar.Attendance', to=settings.AUTH_USER_MODEL),
        ),
    ]
