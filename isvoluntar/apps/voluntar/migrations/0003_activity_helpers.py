# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voluntar', '0002_auto_20151121_1829'),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='helpers',
            field=models.IntegerField(default=1),
        ),
    ]
