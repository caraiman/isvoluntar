# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voluntar', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='activity',
            name='picture',
            field=models.ImageField(upload_to=b'', blank=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='lat',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='activity',
            name='lon',
            field=models.FloatField(null=True, blank=True),
        ),
    ]
