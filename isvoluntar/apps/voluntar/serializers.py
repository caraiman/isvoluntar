from rest_framework import serializers

from globe.serializers import UserSerializer
from voluntar.models import Activity, Attendance


class ActivitySerializer(serializers.ModelSerializer):
    owner = UserSerializer()
    attendees = UserSerializer(many=True)

    class Meta:
        model = Activity


class AttendanceSerializer(serializers.ModelSerializer):
    activity = ActivitySerializer()
    user = UserSerializer()

    class Meta:
        model = Attendance
