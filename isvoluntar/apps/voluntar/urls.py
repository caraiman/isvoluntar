from django.conf.urls import url

from voluntar.views import ActivityDetail, ActivityList, AttendanceCreate,\
                           AttendanceUpdate


urlpatterns = [
    url(r'^activities/$', ActivityList.as_view(), name='activity-list'),
    url(r'^activities/(?P<pk>[0-9]+)/$', ActivityDetail.as_view(),
        name='activity-detail'),
    url(r'^attendances/$', AttendanceCreate.as_view(), name='attendance-create'),
    url(r'^attendances/(P<uid>[0-9]+)/$', AttendanceUpdate.as_view(),
        name='attendance-update')
]