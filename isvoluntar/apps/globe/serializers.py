from django.apps import apps
from django.conf import settings

from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = apps.get_model(settings.AUTH_USER_MODEL)
        fields = ('id', 'username', 'email')