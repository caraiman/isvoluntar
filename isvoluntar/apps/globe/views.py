from django.apps import apps
from django.conf import settings

from rest_framework import generics, permissions

from globe.serializers import UserSerializer


user_model = apps.get_model(settings.AUTH_USER_MODEL)


class UserList(generics.ListAPIView):
    queryset = user_model.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)


class UserDetail(generics.RetrieveAPIView):
    queryset = user_model.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsAuthenticated,)
