from django.conf.urls import url, include
from django.contrib import admin


urlpatterns = [
    url(r'^accounts/', include('allauth.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('globe.urls')),
    url(r'^api/', include('profiles.urls')),
    url(r'^api/', include('voluntar.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
